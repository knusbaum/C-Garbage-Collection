#include <stdio.h>
#include <pthread.h>
#include "../src/kgc.h"



extern char  edata, end;

void **y;

void ** foo(void **zs) {
    static void ** z;
    z= zs;
    printf("Address of z: %p\n", &z);
    return NULL;
}

void *alloc_ten_million(void *arg) {
    GC_INIT;

    char *x;
    unsigned long i;
    for(i = 0; i < 1000000; i++) {
        x = gc_malloc(10000);
        if(x == NULL) {
            printf("Failed to allocate!\n");
        }
        for(int j = 0; j < 100; j++) {
            x[j] = j;
        }
    }

    gc_exit();
    printf("Done allocating.\n");
    return NULL;
}

int main(void) {

    GC_INIT;
    gc_collect();

    void ** sos;
    asm("mov %%rbp, %0" : "=r" (sos));
    printf("Base Address: %p\n", sos);

    void **x = NULL;

    printf("x @ %p\n", &x);

    int i;
    for(i = 0; i < 5; i++) {
        void * nextx = gc_malloc(8);
        if(x) {
            void *prevx = x;
            x = nextx;
            *x = prevx;
            prevx = NULL;
        }
        else {
            x = nextx;
        }
        nextx = NULL;
        printf("Allocated memory @ %p\n", x);
    }

    printf("x: %p\n", x);
    printf("x points to: %p\n", *x);

    printf("\nDoing first collection.\n");
    gc_collect();

    printf("\nNULLing *x\n");

    *x = NULL;

    gc_collect();

    printf("\nChecking global vars. Setting y = x. NULLing x\n");
    printf("Address of y: %p\n", &y);
    y = x;
    x = NULL;

    gc_collect();

    printf("\nChecking inline static vars. z = y; NULLing y\n");
    foo(y);
    y = NULL;

    gc_collect();

    printf("\nClearing z and deleting rest of memory.\n");
    foo(NULL);

    gc_collect();

    printf("\nGoing to test threading.\n");
    pthread_t other_thread;
    pthread_create(&other_thread, NULL, alloc_ten_million, NULL);
    alloc_ten_million(NULL);

    printf("\nGoing to join the other thread now.\n");
    pthread_join(other_thread, NULL);
    printf("DONE!\n");

    gc_exit();

    int *g = gc_malloc(100);
    if(g) {
        printf("g is: %p\n", g);
    }
    else {
        printf("g is NULL!\n");
    }

    GC_INIT;
    g = gc_malloc(100);
    gc_unforget(g);
    gc_forget(g);
    gc_unforget(g);
    gc_unforget(g);

}
