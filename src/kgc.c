#include "kgc.h"
#include "stack.h"
#include <stdio.h>

//#define DEBUG

#ifdef DEBUG
#define DEBUG_OUT(...) fprintf (stderr, __VA_ARGS__)
#else
#define DEBUG_OUT(...)
#endif

struct header_s {
    size_t size;
    struct memnode *prev;
    struct memnode *next;
    short mark;
};

struct memnode {
    struct header_s header;
    char block[];
};

struct stacknode {
    struct memnode *node;
    struct stacknode *next;
};

void gc_init(void ** sos) {
    stack_init_mon(sos + sizeof (void *));
}

__thread struct memnode *head = NULL;
__thread unsigned long alloc_count = 0;

void *gc_malloc(size_t n) {

    if(stack_get_base() == NULL) {
        // Don't allocate without GC_INIT
        return NULL;
    }

    //if(alloc_count >= 500000) {
    if(alloc_count >= 5000) {
        gc_collect();
    }
    alloc_count++;

    void *newmem = malloc(n + sizeof (struct memnode));
    if(newmem) {
        struct memnode *newnode = (struct memnode *)newmem;
        newnode->header.size = n;
        newnode->header.prev = NULL;
        newnode->header.next = head;
        newnode->header.mark = 0;
        if(head) head->header.prev = newnode;
        head = newnode;
        return newnode->block;
    }
    return NULL;
}

static void remove_memnode(struct memnode *node) {
    if(node->header.prev) {
        node->header.prev->header.next = node->header.next;
    }
    else {
        head = node->header.next;
    }

    if(node->header.next) {
        node->header.next->header.prev = node->header.prev;
    }
}

void gc_forget(void *m) {
    struct memnode *current = head;
    while(current) {
        if( m == &current->block ) {
            DEBUG_OUT("Forgetting memnode @ %p\n", current);
            remove_memnode(current);
            return;
        }
        current = current->header.next;
    }
}

void gc_unforget(void *m) {
    struct memnode *node = (struct memnode *)((char *)m - sizeof (struct header_s));
    if(gc_is_still_allocated(m)) {
        DEBUG_OUT("Already have memnode @ %p\n", node);
        return;
    }

    DEBUG_OUT("Un-forgetting node @ %p\n", node);
    node->header.next = head;
    head = node;
}

void gc_free(void *m) {

}

static void clear_marks() {
    struct memnode * current = head;
    while(current) {
        current->header.mark = 0;
        current = current->header.next;
    }
}

static void heap_search_bfs(struct stacknode **to_search) {
    struct stacknode *searching = *to_search;
    if(!searching) return;

    *to_search = (*to_search)->next;

    struct memnode * current = head;
    while(current) {
        if(current->header.mark) {
            current = current->header.next;
            continue;
        }

        void **start = (void **)searching->node->block;
        void **end = (void **)(searching->node->block + searching->node->header.size);

        for(; start < end; start++) {
            if(current->header.mark == 0
               && *start >= (void *)current->block
               && *start < (void *)(current->block + current->header.size)) {
                DEBUG_OUT("Found reference to %p in heap member %p\n", current->block, searching->node->block);
                current->header.mark = 1;
                struct stacknode *new_search = malloc(sizeof (struct stacknode));
                new_search->node = current;
                new_search->next = *to_search;
                *to_search = new_search;
            }
        }
        current = current->header.next;
    }
    free(searching);
    heap_search_bfs(to_search);
}

#if defined __gnu_linux__ || defined __unix__
extern char  edata, end;
#endif

static void data_segment_search(struct stacknode **to_search) {
    #if defined __gnu_linux__ || defined __unix__
    
    char *char_start = &edata;
    char_start -= (long)char_start % sizeof (void *);
    void  **start = (void **)char_start;

    struct memnode * current = head;
    while(current) {
        for(; start < (void **)&end; start++) {
            if(current->header.mark == 0
               && *start >= (void *)current->block
               && *start < (void *)(current->block + current->header.size)) {
                DEBUG_OUT("Found reference to %p @ data segment %p\n", current->block, start);
                current->header.mark = 1;
                struct stacknode *new_search = malloc(sizeof (struct stacknode));
                new_search->node = current;
                new_search->next = *to_search;
                *to_search = new_search;
            }
        }
        current = current->header.next;
    }
    #endif
}

static void stack_search(struct stacknode **to_search, void **top_of_stack) {
    void **bos = stack_get_base();
    
    struct memnode * current = head;
    while(current) {
        
        for(; top_of_stack < bos; top_of_stack++) {
            if(current->header.mark == 0
               && *top_of_stack >= (void *)current->block
               && *top_of_stack < (void *)(current->block + current->header.size)) {
                DEBUG_OUT("Found reference to %p @ %p\n", current->block, top_of_stack);
                current->header.mark = 1;
                struct stacknode *new_search = malloc(sizeof (struct stacknode));
                new_search->node = current;
                new_search->next = *to_search;
                *to_search = new_search;
            }
        }
        current = current->header.next;
    }
}

static void mark(void ** top_of_stack) {

    struct stacknode *to_search = NULL;

    clear_marks();
    DEBUG_OUT("Searching stack! %p\n", top_of_stack);
    stack_search(&to_search, top_of_stack);

    DEBUG_OUT("Searching data segment!\n");
    data_segment_search(&to_search);

    DEBUG_OUT("Searching heap!\n");
    heap_search_bfs(&to_search);
}

static void sweep() {
    struct memnode *current = head;
    while(current) {
        struct memnode *next = current->header.next;
        if(!current->header.mark) {
            remove_memnode(current);
            free(current);
        }
        current = next;
    }
}

static void gc_collect_internal(void **tos) {
    DEBUG_OUT("\nDoing GC!\n");

    DEBUG_OUT("Marking!\n");
    mark(tos);

    DEBUG_OUT("Sweeping!\n");
    sweep();

    DEBUG_OUT("Done collecting!\n\n");
}

void gc_collect() {
    alloc_count = 0;
    gc_collect_internal(stack_get_top());
}

void gc_exit() {
    struct memnode * current = head;
    while(current) {
        head = current->header.next;
        DEBUG_OUT("Freeing %p\n", current);
        free(current);
        current = head;
    }
    head = NULL;
    stack_stop_mon();
}

unsigned long gc_alloc_count() {
    unsigned long count = 0;
    struct memnode * current = head;
    while(current) {
        count++;
        current = current->header.next;
    }
    return count;
}

int gc_is_still_allocated(void *m) {
    struct memnode *current = head;
    while(current) {
        if( m == &current->block ) {
            return 1;
        }
        current = current->header.next;
    }
    return 0;
}
