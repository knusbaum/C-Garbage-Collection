#include <stdlib.h>
#include "stack.h"

static __thread void **start_of_stack = NULL;

void stack_init_mon(void **sos) {
    if(start_of_stack == NULL) {
        start_of_stack = sos;
    }
}

void stack_stop_mon() {
    start_of_stack = NULL;
}

void **stack_get_base() {
    return start_of_stack;
}

void **stack_get_top() {
    void **top_of_stack;
    asm("mov %%rsp, %0" : "=r" (top_of_stack));
    return top_of_stack;
}
