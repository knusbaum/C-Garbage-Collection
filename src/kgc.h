#ifndef GC_H
#define GC_H

#include <stdlib.h>
//#include "stack.h"

//        asm("mov %%rbp, %0" : "=r" (sos));
#define GC_INIT                                    \
    do {                                           \
        void * sos = NULL;                         \
        gc_init(&sos);                             \
    } while (0);

/*
 * DO NOT CALL THIS
 */
void gc_init(void ** sos);


/*
 * Allocate n bytes of memory.
 */
void *gc_malloc(size_t n);

/*
 * Forget about a chunk of memory.
 * It must be manually freed because the
 * garbage collector will not pay attention
 * to it anymore.
 */
void gc_forget(void *m);

/*
 * Un-forget a chunk of memory.
 * It must have been gc_malloc'd and had
 * gc_forget() called on it.
 */
void gc_unforget(void *m);

/*
 * Manually free a chunk of memory.
 */
void gc_free(void *m);

/*
 * Manually trigger the garbage collector.
 */
void gc_collect();

/*
 * Not necessary. Frees ALL allocated memory on a thread.
 */
void gc_exit();

/*
 * Returns the number of allocations that have not been freed.
 */
unsigned long gc_alloc_count();

/*
 * Returns 1 if m is still allocated, else 0
 */
int gc_is_still_allocated(void * m);

#endif
