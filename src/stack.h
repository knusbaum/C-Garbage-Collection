#ifndef STACK_H
#define STACK_H

void stack_init_mon(void **sos);
void stack_stop_mon();
void **stack_get_base();
void **stack_get_top();

#endif
