GCC=cc
CFLAGS=-ggdb -O2 -std=gnu11 -I/usr/local/include
TFLAGS=-O0 -std=gnu11 -ggdb
LFLAGS= -lpthread -L/usr/local/lib -I/usr/local/include
EXECUTABLE=gc_sample

CFILES=$(shell find src -name "*.c")
OBJECTS=$(patsubst %.c,%.o,$(CFILES))
DEPFILES=$(patsubst %.c,./deps/%.d,$(CFILES))

TESTFILES=$(shell find tests -name "*.c")
TOBJECTS=$(patsubst %.c,%.o,$(TESTFILES))

APPFILES=$(shell find app -name "*.c")
APPOBJECTS=$(patsubst %.c,%.o,$(APPFILES))
APPDEPFILES=$(patsubst %.c,./deps/%.d,$(APPFILES))

LIBNAME=kgc


all : $(EXECUTABLE)

-include $(APPDEPFILES)
-include $(DEPFILES)


$(EXECUTABLE) : lib$(LIBNAME).a $(APPOBJECTS)
	@echo -e "[LD]\t\t" $(EXECUTABLE)
	@$(GCC)  $(CFLAGS) $(APPOBJECTS) -L. -Wl,-Bstatic -l$(LIBNAME) -Wl,-Bdynamic $(LFLAGS) -o $(EXECUTABLE)

lib : lib$(LIBNAME).a lib$(LIBNAME).so

lib$(LIBNAME).a : $(OBJECTS) 
	@echo -e "[AR]\t\t" lib$(LIBNAME).a
	@ar rcs lib$(LIBNAME).a $(OBJECTS)

lib$(LIBNAME).so : $(OBJECTS)
	@echo -e "[LD]\t\t" lib$(LIBNAME).so
	@$(CC) -shared -o lib$(LIBNAME).so $(OBJECTS)

test : $(OBJECTS) $(TOBJECTS)
	@echo -e "[LD]\t\t" $(OBJECTS) $(TOBJECTS) test
	@$(GCC) $(TFLAGS) $(LFLAGS) -lcheck $(OBJECTS) $(TOBJECTS) -o test
	-@./test
	@rm test

install : lib
	@mkdir -p /usr/local/include

	@echo -e "[CP]\t\t" kgc.h
	@cp src/kgc.h /usr/local/include/

	@mkdir -p /usr/local/lib

	@echo -e "[CP]\t\t" libkgc.so
	@cp libkgc.so /usr/local/lib/

	@echo -e "[CP]\t\t" libkgc.a
	@cp libkgc.a /usr/local/lib/

deps/%.d : %.c
	@mkdir -p deps
	@mkdir -p `dirname $@` 
	@echo -e "[MM]\t\t" $@
	@$(CC) $(CFLAGS) -MM $< -MF $@

src/%.o : src/%.c deps/src/%.d
	@echo -e "[CC]\t\t" $^
	@$(GCC) $(CFLAGS) -fpic -c $< -o $@

app/%.o : app/%.c deps/app/%.d
	@echo -e "[CC]\t\t" $^
	@$(GCC) $(CFLAGS) -c $< -o $@

clean :
	-@rm -R *.o deps $(EXECUTABLE) lib$(LIBNAME).a lib$(LIBNAME).so test #>>/dev/null 2>&1
	-@find . -name '*.o' -delete
	-@find . -name '*~' -delete
