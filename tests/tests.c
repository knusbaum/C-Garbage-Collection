#include <check.h>
#include <pthread.h>
#include <stdio.h>
#include "../src/kgc.h"


START_TEST (find_in_stack) {
    GC_INIT;
    int *x = gc_malloc(sizeof (int));

    gc_collect();
    ck_assert(gc_is_still_allocated(x));
}
END_TEST

START_TEST (find_in_heap) {
    GC_INIT;
    void **x = NULL;

    int alloc_count = 5;

    int i;
    for(i = 0; i < alloc_count; i++) {
        void * nextx = gc_malloc(8);
        if(x) {
            void *prevx = x;
            x = nextx;
            *x = prevx;
            prevx = NULL;
        }
        else {
            x = nextx;
        }
        nextx = NULL;
    }
    gc_collect();

    // Necessary since x can be in register. Need to force it onto the stack.
    // Should find a way to solve this.
    printf("X: %p\n", x);

    ck_assert_int_eq(gc_alloc_count(), alloc_count);
}
END_TEST

START_TEST (collect_from_heap) {
    GC_INIT;
    void **x = NULL;

    int alloc_count = 5;

    int i;
    for(i = 0; i < alloc_count; i++) {
        void * nextx = gc_malloc(8);
        if(x) {
            void *prevx = x;
            x = nextx;
            *x = prevx;
            prevx = NULL;
        }
        else {
            x = nextx;
        }
        nextx = NULL;
    }
    *x = NULL;
    gc_collect();
    ck_assert_int_eq(gc_alloc_count(), 1);
    ck_assert(gc_is_still_allocated(x));
}
END_TEST


void **y;

START_TEST (global_variables) {
    GC_INIT;

    void **x = gc_malloc(8);
    printf("%p\n", &x);
    gc_collect();
    ck_assert_int_eq(gc_alloc_count(), 1);

    y = x;
    x = NULL;
    printf("Y: %p\n", &y);
    gc_collect();
    ck_assert_int_eq(gc_alloc_count(), 1);

    y = NULL;
    gc_collect();
    ck_assert_int_eq(gc_alloc_count(), 0);
}
END_TEST

void ** static_test(void **zs) {
    static void ** z;
    z = zs;
    printf("%p\n", z);
    return z;
}

START_TEST (static_variables) {
    GC_INIT;

    void **x = gc_malloc(8);
    printf("%p\n", &x);
    ck_assert_int_eq(gc_alloc_count(), 1);

    static_test(x);
    x = NULL;
    gc_collect();
    ck_assert_int_eq(gc_alloc_count(), 1);

    static_test(NULL);
    gc_collect();
    ck_assert_int_eq(gc_alloc_count(), 0);
}
END_TEST

void *hundred_thousand(void *arg) {
    GC_INIT;

    char *x;
    unsigned long i;
    for(i = 0; i < 100000; i++) {
        x = gc_malloc(10000);
        for(int j = 0; j < 100; j++) {
            x[j] = j;
        }
    }
    ck_assert_int_le(gc_alloc_count(), 5001);
    ck_assert_int_ge(gc_alloc_count(), 1);
    gc_exit();
    ck_assert_int_le(gc_alloc_count(), 0);
    return NULL;
}

START_TEST (threading) {
    // This is basically just fuzzing to make sure nothing blows up.
    pthread_t other_thread;
    pthread_create(&other_thread, NULL, hundred_thousand, NULL);
    hundred_thousand(NULL);

    pthread_join(other_thread, NULL);

}
END_TEST

START_TEST (forgetting) {
    GC_INIT;

    // Make sure we get an object.
    void *g = gc_malloc(100);
    ck_assert(gc_is_still_allocated(g));

    // Unforget should do nothing if the object is
    // already known
    gc_unforget(g);
    ck_assert(gc_is_still_allocated(g));
    ck_assert_int_eq(gc_alloc_count(), 1);

    // Forgetting should remove the object from gc's
    // knowledge.
    gc_forget(g);
    ck_assert(!gc_is_still_allocated(g));
    ck_assert_int_eq(gc_alloc_count(), 0);

    // Unforgetting should put the object back in gc's
    // knowledge.
    gc_unforget(g);
    ck_assert_int_eq(gc_alloc_count(), 1);
    ck_assert(gc_is_still_allocated(g));
    ck_assert_int_eq(gc_alloc_count(), 1);

    // Subsequent unforget should also do nothing.
    gc_unforget(g);
    ck_assert(gc_is_still_allocated(g));
    ck_assert_int_eq(gc_alloc_count(), 1);
}
END_TEST

START_TEST (init_exit) {
    int *g = gc_malloc(100);
    ck_assert_ptr_eq(g, NULL);
    ck_assert_int_eq(gc_alloc_count(), 0);

    GC_INIT;
    g = gc_malloc(100);
    ck_assert_ptr_ne(g, NULL);
    ck_assert_int_eq(gc_alloc_count(), 1);

    gc_exit();
    g = gc_malloc(100);
    ck_assert_ptr_eq(g, NULL);
    ck_assert_int_eq(gc_alloc_count(), 0);
}
END_TEST

void * recipient(void *x) {
    GC_INIT;
    gc_unforget(x);
    gc_collect();
    ck_assert_int_eq(gc_alloc_count(), 1);

    // Need to fix this. Not a *huge* deal.
//    x = NULL;
//    gc_collect();
//    printf("X: %p\n", &x);
//    ck_assert_int_eq(gc_alloc_count(), 0);

    return NULL;
}

START_TEST (pass_between_threads) {
    GC_INIT;
    void *x = gc_malloc(10);
    gc_forget(x);

    pthread_t other_thread;
    pthread_create(&other_thread, NULL, recipient, x);
    pthread_join(other_thread, NULL);
}
END_TEST

Suite * gc_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("GC");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, find_in_stack);
    tcase_add_test(tc_core, find_in_heap);
    tcase_add_test(tc_core, collect_from_heap);
    tcase_add_test(tc_core, global_variables);
    tcase_add_test(tc_core, static_variables);
    tcase_add_test(tc_core, threading);
    tcase_add_test(tc_core, forgetting);
    tcase_add_test(tc_core, init_exit);
    tcase_add_test(tc_core, pass_between_threads);

    suite_add_tcase(s, tc_core);

    return s;
}

int main(void) {
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = gc_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
